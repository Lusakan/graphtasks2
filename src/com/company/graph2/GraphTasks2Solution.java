package com.company.graph2;

import java.util.*;

public class GraphTasks2Solution implements GraphTasks2 {
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        HashMap<Integer, Integer> resMap = new HashMap<>();
        boolean[] arr = new boolean[adjacencyMatrix.length];
        arr[startIndex] = true;


        //Заполняю мапу значениями из точки старта
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            resMap.put(i, adjacencyMatrix[startIndex][i]);
        }
        System.out.println(resMap);

        //Нахожу номер вершины у которой наименьший вес с вершиной старта
        int nextIndex = findMinIndex(adjacencyMatrix[startIndex]);


        // Весь алгоритм, работает до тех пор пока не пройдем по всем вершинам
        for (int j = 0; j < adjacencyMatrix.length && !isAllVertexesDone(arr); j++) {
            for (int i = 0; i < adjacencyMatrix.length; i++) {
                // Переменная отвечает за то, что ребро между вершинами nextIndex и i существует, и что мы еще не были
                // вершине i
                boolean b = adjacencyMatrix[nextIndex][i] != 0 && !arr[i];
                if (resMap.get(i) == 0) {
                    if (b) {
                        resMap.replace(i, adjacencyMatrix[nextIndex][i] + resMap.get(nextIndex));
                    }
                } else if (adjacencyMatrix[nextIndex][i] + resMap.get(nextIndex) < resMap.get(i) && b) {
                    resMap.replace(i, adjacencyMatrix[nextIndex][i] + resMap.get(nextIndex));
                }
            }
            arr[nextIndex] = true;
            nextIndex = findMinIndex(resMap, arr);
        }
        return resMap;
    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        HashSet<Integer> pickedVertexesSet = new HashSet<>();
        int weight = 0;
        int vNum = adjacencyMatrix.length;
        pickedVertexesSet.add(0);
        for (int i = 0; i < vNum - 1; i++) {
            int minValue = Integer.MAX_VALUE;
            int index = 0;
            for (int e : pickedVertexesSet) {
                for (int j = 0; j < vNum; j++) {
                    if (adjacencyMatrix[e][j] < minValue && adjacencyMatrix[e][j]!= 0 && !pickedVertexesSet.contains(j)) {
                        minValue = adjacencyMatrix[e][j];
                        index = j;
                    }
                }
            }
            pickedVertexesSet.add(index);
            weight += minValue;
        }
        return weight;
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        int weight = 0;
        int vertNum = adjacencyMatrix.length;
        int[] vertexesArr = new int[vertNum];
        int[] vertexesInArrCount = new int[vertNum];
        List<Edge> edgeSet = new ArrayList<>();

        for (int i = 0; i < vertNum; i++) {
            vertexesArr[i] = i;
        }

        for (int i = 0; i < vertNum; i++) {
            for (int j = 0; j < vertNum; j++) {
                if (adjacencyMatrix[i][j] != 0) {
                    int min = Math.min(i, j);
                    int max = Math.max(i, j);
                    Edge e = new Edge(adjacencyMatrix[i][j], min, max);
                    if (!edgeSet.contains(e)) {
                        edgeSet.add(e);
                    }
                }
            }
        }
        edgeSet.sort(new EdgeComparator());

        for (Edge e : edgeSet) {
            if (toOneSet(e.getFirstVertex(), e.getSecondVertex(), vertexesArr, vertexesInArrCount)) {
                weight += e.getWeight();
            }
        }
        return weight;
    }

    //=========ВСПОМОГАТЕЛЬНЫЕ МЕТОДЫ============
    private boolean isAllVertexesDone(boolean[] arr){
        for (boolean b : arr) {
            if(!b) return false;
        }
        return true;
    }

    private int findMinIndex(Map<Integer, Integer> hm, boolean[] arr){
        int resIndex = -1;
        int min = -1;
        for (int i = 0; i < hm.size(); i++) {
            if(hm.get(i) != 0 && !arr[i]){
                min = hm.get(i);
                resIndex = i;
                break;
            }
        }
        for (int i = 0; i < hm.size(); i++) {
            if(hm.get(i) != 0 && hm.get(i) < min && !arr[i]){
                min = hm.get(i);
                resIndex = i;
            }
        }
        return resIndex;
    }

    private int findMinIndex(int[] matrix){
        int resIndex = -1;
        int min = -1;
        for (int i = 0; i < matrix.length; i++) {
            if(matrix[i] != 0){
                min = matrix[i];
                resIndex = i;
                break;
            }
        }
        for (int i = 0; i < matrix.length; i++) {
            if(matrix[i] != 0 && matrix[i] < min){
                min = matrix[i];
                resIndex = i;
            }
        }
        return resIndex;
    }

    int vertexSet(int vert, int[] arr) {
        return vert == arr[vert] ? vert : vertexSet(arr[vert], arr);
    }

    boolean toOneSet(int vert1, int vert2, int[] arr, int[] viaCount) {
        if (vertexSet(vert1, arr) == vertexSet(vert2, arr)) return false;
        if (viaCount[arr[vert1]] < viaCount[arr[vert2]]) {
            arr[vert1] = arr[vert2];
            viaCount[arr[vert2]]++;
        } else {
            arr[vert2] = arr[vert1];
            viaCount[arr[vert1]]++;
        }
        return true;
    }
}

class Edge implements Comparable<Edge> {
    private int weight;
    private int firstVertex;
    private int secondVertex;

    public Edge(int weight, int firstVertex, int secondVertex) {
        this.weight = weight;
        this.firstVertex = firstVertex;
        this.secondVertex = secondVertex;
    }

    public int getWeight() {
        return weight;
    }

    public int getFirstVertex() {
        return firstVertex;
    }

    public int getSecondVertex() {
        return secondVertex;
    }

    @Override
    public int compareTo(Edge o) {
        return weight - o.getWeight();
    }

    @Override
    public boolean equals(Object obj) {
        Edge e = (Edge) obj;
        boolean b = firstVertex == e.getFirstVertex() && secondVertex == e.getSecondVertex();
        return b;
    }
}

class EdgeComparator implements Comparator<Edge> {
    @Override
    public int compare(Edge o1, Edge o2) {
        boolean bool = o1.getFirstVertex() == o2.getFirstVertex() && o2.getSecondVertex() == o1.getSecondVertex();
        return bool ? 0 : o1.getWeight() - o2.getWeight();
    }
}